# Storage Providers

## Existing Providers
*Note: see the [README](README) for a more up-to-date list of available providers
- MyCouch/CouchDB (in `DynamicStore.Providers.CouchDB`)
- Local JSON files (in `DynamicStore.Providers.Local`)

## Creating a Provider
The basic upshot of creating a storage provider is as follows:
1. Create a new library
2. Install the matching DynamicStore package
3. Implement `IDynamicProvider`
4. Implement your chosen `IDataStore` objects
5. Publish!

Now, obviously, its not quite that simple, so lets step through in a little more detail

### Create a new library
This bit is pretty simple, just create a new project however you want, but the output should be a single Class Library (.dll). Note, however, that DynamicStore _doesn't_ yet support the new ASP.NET/.NET Core Class Libraries, and is primarily aimed at .NET 4.5-based platforms

### Install the matching DynamicStore package
At this time, DynamicStore is available in three flavours:

- `DynamicStore.Pcl` for portable class libraries
- `DynamicStore.Net45` for "standard" .NET 4.5 applications, including ASP.NET
- `DynamicStore.Universal` for "Universal" Windows applications. Currently identical to the `Pcl` version

Install the package most closely matching your own provider's target framework, and you will now be setup and ready to go

### Implement `IDynamicProvider`
This is the main access point for your new provider and is the class that users will pass to the `DynamicStoreEngine`, so make the name good! It should end with `Provider` or `StorageProvider` to avoid confusion. So what's included:

#### Id
This will be what is passed to the `IConfigProvider` if the user decides to use one, so make it unique to your provider. Now for the tricky part here...

*If* your provider should only ever be used once in an application, it's fine to just return a name. *However*, if a user may want to configure multiple instances of your provider (for example, to connect to multiple databases), its best to provide a constructor overload to set this Id. Otherwise, every time a user instantiates your provider, any `IConfigProvider` will return the exact same configuration.

#### Constructors
There are no real restrictions on constructors used in your provider. If you're not sure, the three constructors I would recommend would be:

    SampleProvider() //parameterless for simplicity, if your provider is simple enough
	SampleProvider(DynamicConfiguration config) //for easy, direct configuration without using the Engine's methods
	SampleProvider(string id) //as above in case we want multiple instances with different configurations

#### Repositories
These objects are where all the heavy lifting is really done, and are a bit complex, and implement the main CRUD operations of the provider. There will be better documentation of these interfaces coming soon!

#### `Configure(DynamicConfiguration configuration)`
This method is a pretty important one! The `DynamicStoreEngine` will automatically call this method when the store is being created and passes in its best-matching configuration (either from an `IConfigProvider` or passed in directly) as the `configuration` parameter. This method can therefore be used to do anything you may need to do to setup your provider ready for access. For example, in the CouchDB provider, this method:
- Establishes a connection to the database
- Checks it has all the configuration it needs
- Stores a Client for future requests to use

Whereas in the JSON file provider, we:
- Don't actually really do anything apart from check the configuration for certain keys

It is a reliable method to use for setup since the `Engine` always calls it *once* and even manually creating a DynamicStore should include a call to `Configure()`.

#### DefaultConfiguration
This property is intended for one purpose: to return a "clean", default template of what configuration items your provider expects to find. The ideal `DefaultConfiguration` is one which is identical to the configuration your provider expects but with all the actual values replaced by `null`. Currently, this property is rarely accessed but can be used to confirm a configuration matches its intended provider.

#### ILogger
The `ILogger` object's creation and handling don't need to be worried about in your provider. The main thing this `Log` object provides is a mechanism for logging (duh!) and exceptions! While simply calling `Write()` on the logger is helpful enough, you will also need to provide it when your provider throws an exception since `DynamicStoreException` needs the current `ILogger` to do its work. It's also possible to directly call `RaiseException()` with your exception and it will be handled by the currently configured logging system.

## Implement your chosen `IDataStore`
As mentioned earlier, there will be more documentation on these types coming soon!
<!--TODO-->

## Publish your provider
This may seem like a trivial step to include here, but there's a few important considerations

### Documentation
**Please** include plenty of documentation on your provider. While the basics such as capabilities, limitations and usage are important, you will also need to tell your users what configuration parameters your provider uses. Provide a default/example configuration if you can.

### Conventions
As mentioned above, the standard convention is to follow the naming of the types you are implementing, so that providers end with `Provider` and repositories end with `Repository` etc. As shown in the existing providers, using a namespace of `DynamicStore.Providers.ProviderName` helps with readability and discovery.  
Obviously, none of this is enforced, but consider them strong recommendations.