# app.config Provider

The app.config provider is a simple configruation provider that allows you to easily store arbitrary configuration data in your app.config, making it easy to ship configuration with your code.

## Installation

The app.config provider is in it's own package. Simply run

    Install-Package DynamicStore.Providers.AppConfig
    
to install into your own project. If you haven't already, this will install `DynamicStore` as well.

## Config Provider

Naturally, the app.config provider can only be used to store configuration data, so this package ony exposes the one DynamicStore type: the `AppConfigProvider`, which implements `IConfigProvider`. The constructor accepts an optional `exePath` parameter which can be used to specify the `.exe` you are using for configuration.