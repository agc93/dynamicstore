# Usage I: Creating the Store
For a super-quick rundown of creating a DynamicStore, see the [README](readme)
## Building the store
While its possible to manually create a `DynamicStore` object (thats at the heart of its extensibility), the best (and easiest) way to get started is using the `DynamicStoreEngine`. The `DynamicStoreEngine` is designed as a 'fluent API' to create a new DynamicStore, using method chaining to simplify the process and improve code readability.

When it comes to using the `Engine`, the one major rule is that the call to `UseSimpleStore<>()` or `UseComplexStore()` must come **last**.

Below is a complex example of using the `DynamicStoreEngine` to create a ready-to-go data object.

    var store = new DynamicStoreEngine()
					.EnableStorageProvider(new LocalFileStorageProvider())
					.EnableConfiguration(new AppConfigProvider())
					.EnableCustomLogging(new EventLogWriter())
					.AddNewConfiguration("name", confObject)
					.HideExceptionDetails(true)
					.UseComplexStore();

Now that probably seems like an awful lot of code, but lets step through it one by one

#### .EnableStorageProvider(`IDynamicProvider` storageProvider)
This method accepts an instance of any type implementing IDynamicProvider, and configures the new DynamicStore to use the `storageProvider` as its backing data store.

By design, the `DynamicStore` is now implicitly trusting whatever `IDynamicProvider` object you pass in as being able to handle the data its given and you should thoroughly test any new providers you choose to use.

Most storage providers will give you plenty of documentation, but you may also want to check out the documentation on [Storage Providers](StorageProviders).

#### .EnableConfiguration(`IConfigProvider` configProvider)
Much like its storage provider counterpart, this method accepts an instance of any type implementing `IConfigProvider` and configures the new DynamicStore to use the `configProvider` to hold store configuration data.

Again by design, the `DynamicStore` is implicitly trusting this store to hold configuration data. **_Remember_** this may include passwords and other _sensitive data_, so pick your config providers carefully.

More information on [Config Providers is here](ConfigProviders)

#### .EnableCustomLogging(`ILogger` logger)
This allows you to inject your own logging implementation to easily log DynamicStore issues however you want, as part of your applications wider logging strategy. The `ILogger` interface is deliberately simplistic, so that you can customise how events are logged.

#### .AddNewConfiguration(string name, DynamicConfiguration configuration)
This method is mainly aimed at users who are not using an `IConfigProvider`-based setup. However, it has been retrofitted to support that use case as well, which may lead to a bit of confusion for new users.  

*When using a provider:*  
This method will add the passed configuration to the store of saved configurations. You can call the method repeatedly and when the store is created, all the configurations will be saved to your chosen provider. When the store is created, its configuration will be fetched from the backing store regardless of what you pass in this method.  
*When **not** using a provider*:  
This method will pass the `DynamicConfiguration` object to the StorageProvider's `Configure()` method as an input, so make sure you use the correct configuration object for your chosen Storage Provider. Invoking this multiple times will do nothing and the first configuration will be used.

#### .HideExceptionDetails(bool hideExceptionDetails)
This method is intended as a companion to the custom logging method mentioned earlier. This value defaults to `false`. When set to `true`, DynamicStore will intercept exceptions raised from within the `DynamicStore` object or from any of the configured providers and will instead throw a creatively-named `DynamicException` with the original exception's Message. When set to `false` (the default), exceptions are thrown in their original form and bubbled up the stack 'as-is'.

There's more information on Exceptions on [the wiki page](Exceptions).

### Stores

#### .UseComplexStore();
As mentioned previously, this method returns the actual `DynamicStore` object that you can use to access your data. Remember this store can be used to access any type and most methods are qualified with type parameters.

#### .UseSimpleStore<T>();
It's not shown here, but the `UseSimpleStore<T>()` method is the alternative to `UseComplexStore()` which returns an `ISimpleStore<T>`-based object. You can use this to do many of the same things as a `DynamicStore`, but with just the one type.

#### Find more info [here](DynamicStore)