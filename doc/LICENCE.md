# Licence and Credits

## Project Licence
This project, including all original code, documentation, samples, assets and resources are made available under the MIT licence, permitting free use and modification with attribution, as detailed in the [full licence](http://opensource.org/licenses/MIT).

Third-party components, assets and libraries are made available under the original author's licence and are used here under those same terms. 

## Credits

Project and library icons are provided free by the [Icons8 project](https://icons8.com)

The provided MyCouch provider is based upon Daniel Wertheim's library of the same name. This library is also available under the MIT Licence as documented at [the project homepage](https://github.com/danielwertheim/)  
_Side note: This library also served as a heavy inspiration for DynamicStore's overall design, so thanks Daniel!_

This project also uses the ubiquitous and excellent JSON.NET library from James Newton/Newtonsoft.