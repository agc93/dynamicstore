# Local File Provider

The local file provider is one of the most basic DynamicStore providers and is also probably the easiest to use.

## Installation
The local provider is in it's own package. Simply run

    Install-Package DynamicStore.Providers.Local
    
to install into your own project. If you haven't already, this will install `DynamicStore` as well.

## Storage Provider

The local file storage provider can be used the same as any other storage provider: just use `EnableStorageProvider` with your `DynamicStoreEngine` and provide a configuration.

### Configuration

The storage provider can actually be used without any configuration at all, but it may lead to unpredictable results. It is recommended to specify two configuration parameters:

|Name|Summary|
|:--:|:------|
|`file:FilePrefix`|Used to add a prefix on to stored data files. Defaults to `'data-'`.|
|`file:FolderPrefix`|Defines the folder where data files are stored. Can be a full or relative path. Basically if you can pass it to `System.IO`, you can use it here. Defaults to `'./Objects'`|

### Usage

There's nothing particularly special about this provider, so use it as per the standard DynamicStore APIs and the file access should be pretty transparent.

### Warnings

Make sure your user (or your app's user) account has write permissions to the data directory, as DynamicStore won't attempt to correct this.

## Configuration Provider

This configuration provider can be used with or without the corresponding storage provider. Remember, you are free to mix and match most combinations of file and storage providers in DynamicStore.

This provider doesn't really need (or even accept) any configuration. It will read and write config information into local files in the `./Configuration` folder, suffixing files with `'-config.json'`.

As with the storage provider, make sure you set permissions appropriately.

