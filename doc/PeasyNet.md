# Peasy.NET support

DynamicStore includes handy, built-in support for the Peasy.NET middle tier framework. Much like DynamicStore, Peasy.NET supports swappable data proxies, in a similar way to DynamicStore's storage providers. This leads to a clean and simple API surface for users, regardless of the method of data storage. 

As such, we have created a Peasy.NET-compatible data proxy, that uses a DynamicStore object as the backing storage.

## But why?

The main reasons are three-fold:

### Improved data handling in Peasy.NET

DynamicStore has been designed from the outset to be simple and easy, sometimes to a fault. This means that many data storage and handling features have to be built by the developer, such as concurrency support and, most notably, business rules. DynamicStore doesn't support any of these, but Peasy.NET does. This new provider means you can build apps using Peasy.NET's impressive rules engine and all the middle-tier goodness it brings, and still use DynamicStore for the storage.

### Provider swappability

Peasy.NET is built on the concept of being able to change data proxies at any point. DynamicStore is built on the concept of being able to change storage providers at any point. See what that means? Combine the two and you can easily swap out DynamicStore for any other proxy, or just change your chosen storage provider without any changes to your application layer whatsoever.

### Providers

While we're big fans of Peasy.NET, and its awesome decoupled architecture, the limited quantity of providers is a bit of a downer. While we could reimplement the packaged DynamicStore providers as Peasy.NET proxies, that's duplication of effort that little projects like this one can't afford. This way, we immediately double our supported storage backends, while maintaining less code.

## Caveats and usage

This provider is available in the `DynamicStore.Providers.Peasy` package. Despite the name, there are no DynamicStore providers in this package, but it does include three all-important types:

- `DynamicDomainObject`
- `DynamicDataProxy<T>`
- `DynamicStoreService<T>`

### DynamicDomainObject

Much like the standard `IDynamicObject` (in fact, it implements it), this type is what your domain objects should inherit from. Thanks to an unfortunate disagreement regarding the correct casing of `Id`, this abstract class is needed to implement both `IDynamicObject` (DynamicStore requirement) and `IDomainObject<TKey>` (Peasy.NET requirement). Simply inherit your classes from this and override the `ID` property. Easy as that.

Alternatively, if you'd like to avoid inheriting slightly shaky abstract classes, just make sure your object implements **both** `IDomainObject<string>` and `IDynamicObject`.

### DynamicDataProxy

This is a by-the-books implementation of Peasy.NET's data proxy interface to use a `DynamicStore` object (passed in at creation-time).

### DynamicStoreService

Finally, this is the also-by-the-books implemention of Peasy.NET's data service interface. This class is realistically only helpful if you're not going to be using any of Peasy.NET's advanced capabilities such as enforcing business rules, or validation. If you *are* planning on using these, we recommend you create your own service class and just the `DynamicDataProxy` in your custom service.

## Roadmap

In future, we'd like to do a couple more things with this compatibility:

- Clean up the `async` support (IEnumerable vs IList)
- Update to use BusinessService capabilities rather than basic Service and DataProxy implementations
- Clean up the `DynamicDomainObject` requirements. Particularly, that god-awful `Id` vs `ID` thing.
- \[Long-Term] Maybe implement reverse compatibility. i.e. Use Peasy.NET as a backing store for DynamicStore for the ultimate in flexibility. 