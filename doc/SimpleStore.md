# The SimpleStore`<T>`

The `SimpleStore<T>` object is intended to provide a simpler API for simpler use cases. Hence, it is missing a couple of features from the full `DynamicStore`. Both inherit from `IDataStore`, but SimpleStore doesn't include the complex object repository of `DynamicStore` instead opting for a simple set of methods corresponding to each of the CRUD operations.

* `Get()`
* `GetById(string)`
* `Create()`
* `Delete()`
* `Update()`

You still have access to the `Config` and `Provider` properties for more advanced use cases, but `SimpleStore` also features much, well, simpler, type handling since it is restricted to only one type, specified at create-time.

Note also that since the backends are largely the same and providers will often implement the two `IDataStore`'s the same, there won't often be a particularly big performance or resource gain, so the `SimpleStore` is more of a syntactic convenience than anything.