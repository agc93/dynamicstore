# Exception Handling

**_Note_**: Any users who wish to improve the exception handling components in DynamicStore, **please do**! There are a number of issues and design flaws that I haven't had either the time nor skills to address in the best way possible.

Exceptions in DynamicStore are intended to provide the optimum balance of code convenience and debugging information. To this end, there are two main exceptions to know about:

## `DynamicStoreException`
This exception is intended as a base exception type for all exceptions raised from DynamicStore code (or providers). In practice, this means that exceptions will more often be something much more specific (i.e. `MyCouchConflictException`) but these _should_ all inherit from `DynamicStoreException` to make `catch`ing a little easier.

## `Exception`
Yes, the base `System.Exception`. If you get one of these, it means one of two things: 
1. Your chosen provider has been written poorly. They should be raising `DynamicStoreException` instead; **OR**
2. You have configured your provider to use "Generic Exceptions", which is by default configured to throw a `System.Exception` with the real exception in the `InnerException` property.

For the second case, you can override this at any time (even after you've created your store, which we'll assume is in variable `store`) by changing the property:

    store.Provider.Log.UseGenericExceptions;

This change the behaviour of any future exceptions, so remember to either change it back or update your `catch` blocks.

## Handling
The ideal scenario for this design was as follows:

    try {
		var record = store.Objects.Delete(myObject);
	} catch (DynamicStoreException dsEx) {
		//print very helpful error message with StackTrace and everything for developers to use
		//or just for more helpful error pages
	} catch (Exception ex) {
		//print generic user-friendly error message
		//no unfortunate runtime exceptions
	}

In order to make the third case more effective, when throwing generic `Exception`s, the default implementation will still include a message in the `Message` property, and will sometimes include the real exception in the `InnerException` property.

## New Projects
For those on the bleeding edge, this ties in nicely to the new C# 6 feature: conditional exception handling. Using this new feature, we can change the earlier code to be:

    try {
		var record = store.Objects.Delete(myObject);
	} catch (DynamicStoreException dsEx) if (dsEx.InnerException != null) {
		//print very helpful error message with more information on what happened, using InnerException
	} catch (DynamicStoreException dsEx) {
		//more generic error page, with a message regarding an issues with saving for example
	} catch (Exception ex) {
		//print generic user-friendly error message
		//no unfortunate runtime exceptions
	}


## Implementation Tips
- I find it can also be a good idea to conditionally define the `UseGenericExceptions` property to an app setting for easier changes later. For example, using `web.config`'s or `AppSettings`, you could define this property to `true` for Debug modes or `false` for Release builds!
- Remember that the more specific forms of exception are all provider-specific, so trying to `catch` them in your application code is effectively hard-coding yourself into one provider. If you want specific functionality per-provider, you could also use the conditional handling above to check, for example, `catch (DynamicStoreException ex) if (ex is MyCouchDynamicException)`.
