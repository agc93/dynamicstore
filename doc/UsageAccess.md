# Usage II: Data Access
So now you have a fully-fledged `DynamicStore` object? Time to get some data with it!  
*Using a SimpleStore instead? [Read this first](SimpleStore)*


## Overview
For the purposes of this example, I'm going to assume your `DynamicStore` is in a variable called `store`.  
It's worth noting at this point that pretty much everything in DynamicStore is asynchronous, so you will need to await most calls.


## Access
Well, this part is simple. To get all data objects of a given type:

    var myObjects = await store.Objects.Get<T>();

This will return an IList\<T> of all objects of type T in the store. Note that since this is an IList, you can use much of what LINQ has to offer on the results.

If you just want a single object and you know its id:

    var myObject = await store.Objects.GetById<T>("someId");

BAM, `myObject` is now the object you requested, of type `T`. Simple as that.

## Putting the CUD in CRUD

### Creating
To create an object, simply create it in code:

    var newObject = new SampleObject() {
		Name = "MyAwesomeName"
	};

Now, add it to the data store

    var record = store.Objects.Create(newObject);

You may notice I have introduced a variable there: `record`. That's because a few methods will return an `IRecordReference` object to give you some information about the operation. Most providers will use the built-in `JsonRecord` object, which has  a `Raw` property with the raw JSON of the object you created. At the very minimum, all providers will give you the Id and Rev of the object you just created.

### Updating

So after you have created or retrieved an object, you're likely going to want to change it, right? Well, all of that is up to you as you can manipulate your objects as you see fit. Once you're done:

    var record = store.Objects.Update(modifiedObject);

DynamicStore (through the chosen storage provider) will now locate the original object using its `Id` property and seamlessly handle updating/recreation/changing the object for you. You can `await` this call to wait until the save operation has completed.

### Deleting

If you want to delete an object you have retrieved, simply use the `Delete` method:

    var record = await store.Objects.Delete(objectToDelete);

If you don't have the object, you can still delete it from storage by using its type and `Id`:

    var record = await store.Objects.Delete<T>("someId");

Much like modifications and creations, deletions return an `IRecordReference`. Most providers will set the `Rev` property to indicate success or failure (0 for failure, -1 for success), but this behaviour varies provider to provider