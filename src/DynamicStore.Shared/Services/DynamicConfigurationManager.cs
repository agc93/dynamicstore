﻿using DynamicStore.Infrastructure;

namespace DynamicStore.Services
{
    internal class DynamicConfigurationManager : IDynamicConfigurationManager
    {
        public bool Enabled { get; set; }

        public IConfigProvider Source { get; set; }

        public void Load() {
        }

        public void Add(string name, DynamicConfiguration configuration) {
            if (Source != null) Source.AddConfig(name, configuration);
        }
    }
}