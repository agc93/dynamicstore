﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DynamicStore.Services
{
    public static class Extensions
    {
        public static bool IsNotNull(this object o) {
            return o != null;
        }
    }

    public static class TypeExtensions
    {
        public static dynamic Get(this KeyValuePair<Type, object> input) {
            return input.Value;
        }

        public static T To<T>(this KeyValuePair<Type, object> input) {
            if (typeof(T) != input.Key)
                throw new ArgumentException(string.Format("Invalid Type cast! Configuration item type is: {0}",
                    input.Key));
            try {
                return (T)input.Value.To<T>(input.Key);
                //                     ^ this is the one just above here
            }
            catch (Exception) {
                throw new ArgumentException(string.Format("Unexpected error occured when converting from type {0} to type {1}", typeof(T), input.Key));
            }
        }

        public static Task<T> ToTask<T>(this T result) {
            var tcs = new TaskCompletionSource<T>();
            tcs.SetResult(result);
            return tcs.Task;
        }

        private static object To<T>(this object obj, Type input) {
            return (T)Convert.ChangeType(obj, typeof(T));
        }
    }
}