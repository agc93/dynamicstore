﻿using System;
using DynamicStore.Exceptions;
using DynamicStore.Infrastructure;

namespace DynamicStore.Services
{
    internal partial class DefaultLogger : ILogger
    {
        public bool UseGenericExceptions { get; set; }

        private static string Prefix = "EE";

        public void RaiseException(DynamicStoreException exception, string message) {
            if (UseGenericExceptions) {
                throw new Exception(exception.Message, exception);
                //do something
            }
            Write(String.Format("{0} thrown! {1} {2} {1} {3}", exception.ToString(), Environment.NewLine, message, exception.DebugMessage));
            throw exception;
        }

        public void Write(string message, ErrorCondition sev) {
            Prefix = sev.ToPrefix();
            Write(message);
        }
    }
}