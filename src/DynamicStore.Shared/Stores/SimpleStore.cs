﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DynamicStore.Infrastructure;
using DynamicStore.Providers;
using DynamicStore.Services;

namespace DynamicStore.Stores
{
    public class SimpleStore<TEntity> : ISimpleStore<TEntity> where TEntity : class, IDynamicObject
    {
        public SimpleStore(IDynamicConfigurationManager config = null) {
            Config = config ?? new DynamicConfigurationManager();
        }

        private ISimpleRepository<TEntity> Repository { get; set; }

        public IDynamicProvider Provider { get; private set; }

        public void ChangeStorage(IDynamicProvider newProvider) {
            Provider = newProvider;
            Repository = newProvider.SimpleRepository as ISimpleRepository<TEntity>;
        }

        public void ChangeStorage(DynamicBuilder configuration) {
            throw new NotImplementedException();
        }

        public IDynamicConfigurationManager Config { get; private set; }

        public Task<IEnumerable<TEntity>> Get() {
            return Repository.Get();
        }

        public Task<TEntity> GetById(string id) {
            return Repository.GetById(id);
        }

        public Task<IRecordReference> Create(TEntity entity) {
            return Repository.Create(entity);
        }

        public Task<IRecordReference> Delete(string id) {
            return Repository.Delete(id);
        }

        public Task<IRecordReference> Update(TEntity entityToUpdate) {
            return Repository.Update(entityToUpdate);
        }
    }
}