﻿using DynamicStore.Infrastructure;
using DynamicStore.Providers;
using DynamicStore.Services;

namespace DynamicStore.Stores
{
    public partial class DynamicStore : IDynamicStore
    {
        public DynamicStore(IDynamicConfigurationManager config = null) {
            Config = config ?? new DynamicConfigurationManager();
        }

        public IComplexRepository Objects {
            get { return Provider.DynamicRepository; }
        }

        public void ChangeStorage(IDynamicProvider newProvider) {
            Provider = newProvider;
        }

        public IDynamicProvider Provider { get; private set; }

        public void ChangeStorage(DynamicBuilder configuration) {
            //this.Objects = configuration.StoreProviderFn() as IComplexRepository;
        }

        public IDynamicConfigurationManager Config { get; set; }
    }
}