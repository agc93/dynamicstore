﻿namespace DynamicStore.Infrastructure
{
    public interface IDynamicObject
    {
        string Id { get; }

        string Rev { get; set; }
    }
}