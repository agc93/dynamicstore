﻿using System;
using System.Collections.Generic;

namespace DynamicStore.Infrastructure
{
    public interface IDynamicConfigurationManager
    {
        bool Enabled { get; set; }

        IConfigProvider Source { get; set; }

        void Load();

        void Add(string name, DynamicConfiguration configuration);
    }

    public interface IConfigProvider
    {
        DynamicConfiguration GetConfig(string name);

        IList<DynamicConfiguration> GetAllConfigurations();

        Dictionary<string, Type> GetConfigSpecification(string name);

        Dictionary<string, Type> AddConfig(string name, DynamicConfiguration configuration);
    }

    public class DynamicConfiguration : Dictionary<string, KeyValuePair<Type, object>>
    {
    }
}