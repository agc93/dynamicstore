﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DynamicStore.Infrastructure
{
    public interface IDynamicRepository
    {
    }

    public interface IComplexRepository : IDynamicRepository
    {
        Task<IList<TEntity>> Get<TEntity>() where TEntity : class, IDynamicObject;

        Task<TEntity> GetById<TEntity>(string id) where TEntity : class, IDynamicObject;

        Task<IRecordReference> Create<TEntity>(TEntity entity) where TEntity : class, IDynamicObject;

        Task<IRecordReference> Delete<TEntity>(TEntity entityToDelete) where TEntity : class, IDynamicObject;

        Task<IRecordReference> Delete<TEntity>(string id) where TEntity : class, IDynamicObject;

        Task<IRecordReference> Update<TEntity>(TEntity entityToUpdate) where TEntity : class, IDynamicObject;
    }

    public interface ISimpleRepository<TEntity> : IDynamicRepository where TEntity : IDynamicObject
    {
        Task<IEnumerable<TEntity>> Get();

        Task<TEntity> GetById(string id);

        Task<IRecordReference> Create(TEntity entity);

        Task<IRecordReference> Delete(TEntity entityToDelete);

        Task<IRecordReference> Delete(string id);

        Task<IRecordReference> Update(TEntity entityToUpdate);
    }

    public interface IMultiUserRepository : IDynamicRepository
    {
        Task<IList<TEntity>> Get<TEntity>() where TEntity : class, IDynamicObject;

        Task<TEntity> GetById<TEntity>(string id) where TEntity : class, IDynamicObject;

        Task<IRecordReference> Create<TEntity>(TEntity entity) where TEntity : class, IDynamicObject;

        Task<IRecordReference> Delete<TEntity>(TEntity entityToDelete) where TEntity : class, IDynamicObject;

        Task<IRecordReference> Delete<TEntity>(string id) where TEntity : class, IDynamicObject;

        Task<IRecordReference> Update<TEntity>(TEntity entityToUpdate) where TEntity : class, IDynamicObject;
    }
}