﻿namespace DynamicStore.Infrastructure
{
    public interface IDynamicProvider
    {
        string Id { get; }

        IComplexRepository DynamicRepository { get; }

        ISimpleRepository<IDynamicObject> SimpleRepository { get; }

        void Configure(DynamicConfiguration configuration);

        DynamicConfiguration DefaultConfiguration { get; }

        ILogger Log { get; set; }
    }
}