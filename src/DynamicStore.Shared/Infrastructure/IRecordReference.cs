﻿namespace DynamicStore.Infrastructure
{
    public interface IRecordReference
    {
        string Id { get; }
        string Rev { get; }
        
    }
}