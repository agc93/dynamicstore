﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DynamicStore.Providers;

namespace DynamicStore.Infrastructure
{
    public interface IDataStore
    {
        void ChangeStorage(IDynamicProvider newProvider);

        void ChangeStorage(DynamicBuilder configuration);

        IDynamicConfigurationManager Config { get; }

        IDynamicProvider Provider { get; }
    }

    public interface IDynamicStore : IDataStore
    {
        IComplexRepository Objects { get; }
    }

    public interface ISimpleStore<TEntity> : IDataStore where TEntity : class, IDynamicObject
    {
        Task<IEnumerable<TEntity>> Get();

        Task<TEntity> GetById(string id);

        Task<IRecordReference> Create(TEntity entity);

        Task<IRecordReference> Delete(string id);

        Task<IRecordReference> Update(TEntity entityToUpdate);
    }

    public interface IUserDataStore : IDataStore
    {
        IMultiUserRepository Objects { get; }
    }
}