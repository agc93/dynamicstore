﻿using System;
using DynamicStore.Exceptions;

namespace DynamicStore.Infrastructure
{
    public interface ILogger
    {
        void Write(string message);

        void Write(string message, ErrorCondition sev);

        void RaiseException(DynamicStoreException exception, string message);

        bool UseGenericExceptions { get; set; }
    }

    public enum ErrorCondition
    {
        Error,
        Warning,
        Info,
        Debug
    }

    public static class ErrorExtensions
    {
        public static string ToPrefix(this ErrorCondition sev) {
            string prefix = String.Empty;
            switch (sev) {
                case ErrorCondition.Error:
                    prefix = "EE|";
                    break;

                case ErrorCondition.Warning:
                    prefix = "WW|";
                    break;

                case ErrorCondition.Info:
                    prefix = "II|";
                    break;

                case ErrorCondition.Debug:
                    prefix = "DD|";
                    break;

                default:
                    throw new ArgumentOutOfRangeException("sev");
            }
            return prefix;
        }
    }
}