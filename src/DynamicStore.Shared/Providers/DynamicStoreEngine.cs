﻿using System.Collections.Generic;
using DynamicStore.Infrastructure;
using DynamicStore.Services;
using DynamicStore.Stores;

namespace DynamicStore.Providers
{
    public class DynamicStoreEngine
    {
        private readonly Stores.DynamicStore _dynamicStore = new Stores.DynamicStore();

        private IDynamicProvider _storage;
        private IConfigProvider _config;
        private DynamicBuilder _dynBuilder;
        private ILogger _logger;
        private readonly IList<KeyValuePair<string, DynamicConfiguration>> _dynConfig = new List<KeyValuePair<string, DynamicConfiguration>>();
        private bool _genericExceptions;

        public DynamicStoreEngine AddNewConfiguration(string name, DynamicConfiguration configuration) {
            _dynConfig.Add(new KeyValuePair<string, DynamicConfiguration>(name, configuration));
            return this;
        }

        public DynamicStoreEngine EnableConfiguration(IConfigProvider configProvider) {
            _config = configProvider;
            return this;
        }

        public DynamicStoreEngine EnableStorageProvider(IDynamicProvider storageProvider) {
            _storage = storageProvider;
            return this;
        }

        public DynamicStoreEngine EnableStorageProvider(DynamicBuilder builder) {
            _dynBuilder = builder;
            return this;
        }

        public DynamicStoreEngine EnableCustomLogging(ILogger logger) {
            _logger = logger;
            return this;
        }

        public DynamicStoreEngine HideExceptionDetails(bool hideExceptionDetails) {
            _genericExceptions = hideExceptionDetails;
            return this;
        }

        public Stores.DynamicStore UseComplexStore() {
            BuildStore(_dynamicStore);
            return _dynamicStore;
        }

        private void BuildStore(IDataStore store) {
            if (_dynBuilder.IsNotNull()) {
                store.ChangeStorage(_dynBuilder);
            } else {
                store.ChangeStorage(_storage);
            }
            if (_config.IsNotNull()) {
                store.Config.Source = _config;
                store.Config.Enabled = true;
                store.Config.Load();
                foreach (var item in _dynConfig) {
                    store.Config.Add(item.Key, item.Value);
                }
                store.Provider.Configure(store.Config.Source.GetConfig(store.Provider.Id));
            } else {
                store.Config.Enabled = false;
                foreach (var config in _dynConfig) {
                    store.Provider.Configure(config.Value);
                }
            }
            store.Provider.Log = _logger ?? new DefaultLogger()
            {
                UseGenericExceptions = _genericExceptions
            };
        }

        public ISimpleStore<T> UseSimpleStore<T>() where T : class, IDynamicObject {
            var simple = new SimpleStore<T>();
            BuildStore(simple);
            return simple;
        }
    }
}