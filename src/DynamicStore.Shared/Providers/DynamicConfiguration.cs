﻿using System;
using DynamicStore.Infrastructure;
using DynamicStore.Services;

namespace DynamicStore.Providers
{
    public class DynamicBuilder
    {
        public Func<IDynamicRepository> StoreProviderFn { get; set; }

        // ReSharper disable once MemberCanBePrivate.Global
        public Func<IConfigProvider> ConfigFactoryFn { get; set; }

        public Func<ILogger> LoggerFn { get; set; }

        public DynamicBuilder() {
            BuildStoreProvider();
            BuildConfigFactory();
        }

        protected virtual void BuildStoreProvider() {
            StoreProviderFn = () => null;
        }

        protected virtual void BuildConfigFactory() {
            //ConfigFactoryFn = () => new JsonConfigProvider();
            ConfigFactoryFn = () => null;
        }

        protected virtual void BuildLogger() {
            LoggerFn = () => new DefaultLogger();
        }
    }

    public static class DynamicConfigurations
    {
        public static DynamicBuilder Defaults { get; set; }

        static DynamicConfigurations() {
            Defaults = new DynamicBuilder();
        }
    }
}