﻿using DynamicStore.Infrastructure;

namespace DynamicStore.Providers
{
    public class JsonRecord : IRecordReference
    {
        public string DocumentId { get; set; }

        public string Revision { get; set; }

        public string Raw { get; set; }

        public string Id { get { return DocumentId; } }

        public string Rev { get { return Revision; } }
    }
}