﻿using System;
using DynamicStore.Infrastructure;

namespace DynamicStore.Exceptions
{
    public class DynamicStoreException : Exception
    {
        private ILogger Logger { get; set; }

        public string DebugMessage { get; set; }

        public new Exception InnerException { get; set; }

        private string _message;

        public override sealed string StackTrace {
            get { return base.StackTrace; }
        }

        public new string Message {
            get { return _message ?? base.Message; }
            set { _message = value; }
        }

        // ReSharper disable once MemberCanBeMadeStatic.Local
        private void Intitialize() {
            Logger.Write(Message);
            Logger.Write(DebugMessage);
            Logger.Write(StackTrace);
            Logger.RaiseException(this, Message);
        }

        protected DynamicStoreException(ILogger logger) {
            Logger = logger;
            this.Intitialize();
        }

        protected DynamicStoreException(ILogger logger, string message)
            : base(message) {
            Logger = logger;
            Message = message;
            this.Intitialize();
        }

        protected DynamicStoreException(ILogger logger, Exception innerException)
            : base(String.Empty, innerException) {
            Logger = logger;
            Message = innerException.Message;
            this.Intitialize();
        }

        public DynamicStoreException(ILogger logger, Exception innerException, string message)
            : base(message, innerException) {
            Message = message;
            Logger = logger;
            this.Intitialize();
        }
    }
}