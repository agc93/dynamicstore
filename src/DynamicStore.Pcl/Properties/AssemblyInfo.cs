﻿using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("DynamicStore.Pcl")]
[assembly: AssemblyDescription("The PCL version of the core DynamicStore library, including everything you need to get up and running with DynamicStore storage in your application. Targets .NET 4.5 and Windows 8.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Alistair Chapman")]
[assembly: AssemblyProduct("DynamicStore.Pcl")]
[assembly: AssemblyCopyright("Copyright ©  2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguage("en")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("0.9.1.1")]
[assembly: AssemblyFileVersion("0.9.1.1")]
