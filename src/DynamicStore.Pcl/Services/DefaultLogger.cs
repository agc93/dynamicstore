﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicStore.Services
{
    partial class DefaultLogger
    {
        public void Write(string message)
        {
            if (message != null) Debug.WriteLine(message);
        }
    }
}
