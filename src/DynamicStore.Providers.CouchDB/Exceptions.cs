﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DynamicStore.Exceptions;
using DynamicStore.Infrastructure;

namespace DynamicStore.Providers.CouchDB
{
    class MyCouchDynamicException : DynamicStoreException
    {
        protected internal MyCouchDynamicException(ILogger logger) : base(logger)
        {
           
        }
    }

    internal class MyCouchConflictException : MyCouchDynamicException
    {
        protected internal MyCouchConflictException(ILogger logger) : base(logger)
        {
        }
    }

}
