﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyCouch.Responses;

namespace DynamicStore.Providers.CouchDB
{
    public static class Extensions
    {
        public static IList<TEntity> ToValues<TEntity>(
            this ViewQueryResponse<TEntity> response) {
            IList<TEntity> list = response.Rows.Select(row => row.Value).ToList();
            return list;
        }
    }
}