﻿using System;
using System.Collections.Generic;
using System.Text;
using DynamicStore.Infrastructure;
using MyCouch;

namespace DynamicStore.Providers.CouchDB
{
    public class MyCouchDbProvider : IDynamicProvider
    {
        public string Id {
            get { return "mycouch"; }
        }

        public MyCouchDbProvider() {
        }

        public MyCouchDbProvider(DynamicConfiguration configuration) {
            Configure(configuration);
        }

        public IComplexRepository DynamicRepository {
            get {
                if (Client == null) throw new MyCouchDynamicException(Log)
                {
                    Message = "Configuration required! Call Configure() first!",
                    DebugMessage = "Client object null! Configure() needs to populate it first!"
                };
                return new MyCouchRepository(Client, Log);
            }
        }

        [Obsolete("This provider only supports complex stores!", false)]
        public ISimpleRepository<IDynamicObject> SimpleRepository {
            get {
                throw new NotImplementedException("This provider only supports complex stores!");
            }
        }

        public void Configure(DynamicConfiguration configuration) {
            var sb = new StringBuilder();
            sb.Append(configuration["db:Scheme"].Value);
            sb.Append(configuration["db:UserName"].Value);
            sb.Append(":");
            sb.Append(configuration["db:Password"].Value);
            sb.Append("@");
            sb.Append(configuration["db:Uri"].Value);
            var uri = new Uri(sb.ToString());
            var client = new MyCouchClient(new DbConnectionInfo(uri, configuration["db:DbName"].Value.ToString()));
            Client = client;
        }

        public DynamicConfiguration DefaultConfiguration {
            get {
                return new DynamicConfiguration
                {
                    {
                        "db:Scheme", new KeyValuePair<Type, object>(
                            typeof (string),
                            null)
                    },
                    {
                        "db:UserName", new KeyValuePair<Type, object>(
                            typeof (string),
                            null)
                    },
                    {
                        "db:Password", new KeyValuePair<Type, object>(
                            typeof (string),
                            null)
                    },
                    {
                        "db:Uri", new KeyValuePair<Type, object>(
                            typeof (string),
                            null)
                    },
                    {
                        "db:DbName", new KeyValuePair<Type, object>(
                            typeof (string),
                            null)
                    }
                };
            }
        }

        public ILogger Log { get; set; }

        private MyCouchClient Client { get; set; }
    }
}