﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using DynamicStore.Infrastructure;
using DynamicStore.Providers;
using MyCouch;
using MyCouch.Querying;
using MyCouch.Requests;
using MyCouch.Responses;

namespace DynamicStore.Providers.CouchDB
{
    public class MyCouchRepository : IComplexRepository
    {
        private const string DesignDocName = "designDoc";
        private const string DesignViewName = "doctype";
        private ILogger Debug { get; set; }

        public MyCouchRepository(MyCouchClient client, ILogger debug)
        {
            this.Client = client;
            Debug = debug;
            this.EnsureDesignDocsExist();
        }

        private async void EnsureDesignDocsExist()
        {
            try
            {
                await Client.Documents.PostAsync(new PostDocumentRequest(File.ReadAllText("designDoc.json")));
            }
            catch (Exception)
            {
                // ignored
            }
            throw new MyCouchDynamicException(Debug)
            {
                Message = "Error while creating design documents!"
            };
        }

        private MyCouch.MyCouchClient Client { get; set; }

        [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
        [SuppressMessage("ReSharper", "SuggestVarOrType_Elsewhere")]
        public async Task<IList<TEntity>> Get<TEntity>() where TEntity : class, IDynamicObject
        {
            QueryViewRequest query = new QueryViewRequest(DesignDocName, DesignViewName).Configure(q => q
                .IncludeDocs(true)
                .Key(typeof (TEntity).ToString().Split('.').Last().ToLower())
                .Stale(Stale.UpdateAfter)
                );
            try {
                ViewQueryResponse<TEntity> result = await Client.Views.QueryAsync<TEntity>(query);
                if (result.IsEmpty) {
                    Debug.Write("DEBUG: Zero rows returned!");
                    IList<TEntity> ilist = new List<TEntity>();
                    return ilist;
                }
                if (!result.IsSuccess) {
                    Debug.Write("Request failed!");
                    throw new MyCouchDynamicException(Debug)
                    {
                        Message = "There was an error getting records from the database!",
                        DebugMessage = "Response not successful. No exceptions caught from request."
                    };
                }
                Console.WriteLine(result.ToStringDebugVersion());
                return result.ToValues();
            }
            catch (Exception ex) {
                throw new MyCouchDynamicException(Debug)
                {
                    InnerException = ex,
                    DebugMessage = ex.Message
                };
            }
        }

        public async Task<TEntity> GetById<TEntity>(string id) where TEntity : class, IDynamicObject
        {
            var response = await Client.Entities.GetAsync<TEntity>(id);
            return response.Content;
        }

        public async Task<IRecordReference> Create<TEntity>(TEntity entity) where TEntity : class, IDynamicObject
        {
            return await PostEntity(entity);
        }

        public async Task<IRecordReference> Delete<TEntity>(TEntity entityToDelete) where TEntity : class, IDynamicObject
        {
            await DeleteEntity<TEntity>(entityToDelete.Id);
            return new JsonRecord
            {
                DocumentId = entityToDelete.Id,
                Raw = String.Empty,
                Revision = "0"
            };

        }

        public async Task<IRecordReference> Delete<TEntity>(string id) where TEntity : class, IDynamicObject
        {
            await DeleteEntity<TEntity>(id);
            return new JsonRecord()
            {
                DocumentId = id,
                Raw = String.Empty,
                Revision = "0"
            };

        }

        public async Task<IRecordReference> Update<TEntity>(TEntity entityToUpdate) where TEntity : class, IDynamicObject
        {
            var existing = await Client.Entities.GetAsync<TEntity>(entityToUpdate.Id);
            entityToUpdate.Rev = existing.Rev;
            return await PostEntity(entityToUpdate);

        }

        private async Task<IRecordReference> PostEntity<TEntity>(TEntity entity) where TEntity : class {
            //at the moment, this just majorly clobbers whatever document is up there now
            var response = await Client.Entities.PostAsync(entity);
            if (response.IsSuccess) {
                return new JsonRecord()
                {
                    DocumentId = response.Id,
                    Revision = response.Rev,
                    Raw = response.ToStringDebugVersion()
                };
            }
            if (response.StatusCode != HttpStatusCode.Conflict) return null;
            throw new MyCouchConflictException(Debug);
        }

        private async Task<bool> DeleteEntity<TEntity>(string id) where TEntity : class, IDynamicObject
        {
            var existing = await Client.Entities.GetAsync<TEntity>(id);
            var request = new BulkRequest().Delete(existing.Id, existing.Rev);
            var response = await Client.Documents.BulkAsync(request);
            if (response.IsSuccess) {
                return true;
            }
            throw new MyCouchDynamicException(Debug)
            {
                Message = "Error encountered when deleting record"
            };

        }
    }
}
