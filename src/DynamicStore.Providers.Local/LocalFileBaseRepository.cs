﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DynamicStore.Infrastructure;
using DynamicStore.Services;

namespace DynamicStore.Providers.Local
{
    public class LocalFileBaseRepository
    {
        protected static string FolderPrefix {
            set { FileConstants.StorageFolder = value; }
        }

        protected static string FilePrefix {
            set { FileConstants.StorageFolder = value; }
        }

        protected IList<TEntity> Get<TEntity>() where TEntity : class, IDynamicObject {
            IList<TEntity> ilist = FileAccess.Storage.GetObjects<TEntity>().ToList();
            return ilist;
        }

        protected static TEntity GetById<TEntity>(string id) where TEntity : class, IDynamicObject
        {
            return FileAccess.Storage.GetObject<TEntity>(id);
        }

        protected IRecordReference Create<TEntity>(TEntity entity) where TEntity : class, IDynamicObject {
            var storeObject = FileAccess.Storage.StoreObject(entity);
            IRecordReference r = new JsonRecord
            {
                DocumentId = storeObject.Id,
                Revision = storeObject.Rev ?? "0",
                Raw = FileAccess.GetJson(storeObject)
            };
            return r;
        }

        protected IRecordReference Delete<TEntity>(TEntity entityToDelete) where TEntity : class, IDynamicObject
        {
            return DeleteObject<TEntity>(entityToDelete.Id);
        }

        private static IRecordReference DeleteObject<TEntity>(string id) where TEntity : class, IDynamicObject {
            IRecordReference r;
            var result = FileAccess.Storage.RemoveObject<TEntity>(id);
            if (result) {
                r = new JsonRecord
                {
                    DocumentId = id,
                    Revision = "-1"
                };
            } else {
                r = new JsonRecord
                {
                    DocumentId = id,
                    Revision = "0"
                };
            }
            return r;
        }

        protected IRecordReference Delete<TEntity>(string id) where TEntity : class, IDynamicObject
        {
            return DeleteObject<TEntity>(id);
        }

        protected IRecordReference Update<TEntity>(TEntity entityToUpdate) where TEntity : class, IDynamicObject {
            var o = FileAccess.Storage.ReplaceObject(entityToUpdate);
            IRecordReference r = new JsonRecord
            {
                DocumentId = o.Id,
                Revision = o.Rev ?? "0",
                Raw = FileAccess.GetJson(o)
            };
            return r;
        }
    }
}
