using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DynamicStore.Infrastructure;
using DynamicStore.Services;

namespace DynamicStore.Providers.Local
{
    public class LocalFileStorageRepository : IComplexRepository
    {
        public LocalFileStorageRepository(string filePrefix, string folderPrefix, List<Newtonsoft.Json.JsonConverter> converters = null)
        {
            if (filePrefix.IsNotNull()) {
                FilePrefix = filePrefix; 
            }
            if (folderPrefix.IsNotNull()) {
                FolderPrefix = folderPrefix; 
            }
            if (converters != null && converters.Any()) {
                foreach (var converter in converters)
                {
                    FileAccess.Settings.Converters.Add(converter);
                }
            }
        }

        private static string FolderPrefix
        {
            set { FileConstants.StorageFolder = value; }
        }

        private static string FilePrefix
        {
            set { FileConstants.StorageFolder = value; }
        }

        public Task<IList<TEntity>> Get<TEntity>() where TEntity : class, IDynamicObject
        {
            IList<TEntity> ilist = FileAccess.Storage.GetObjects<TEntity>().ToList();
            return ilist.ToTask();
        }

        public Task<TEntity> GetById<TEntity>(string id) where TEntity : class, IDynamicObject
        {
            return FileAccess.Storage.GetObject<TEntity>(id).ToTask();
        }

        public Task<IRecordReference> Create<TEntity>(TEntity entity) where TEntity : class, IDynamicObject
        {
            var storeObject = FileAccess.Storage.StoreObject(entity);
            IRecordReference r = new JsonRecord
            {
                DocumentId = storeObject.Id,
                Revision = storeObject.Rev ?? "0",
                Raw = FileAccess.GetJson(storeObject)
            };
            return r.ToTask();
        }

        public Task<IRecordReference> Delete<TEntity>(TEntity entityToDelete) where TEntity : class, IDynamicObject
        {
            return DeleteObject<TEntity>(entityToDelete.Id).ToTask();
        }

        private static IRecordReference DeleteObject<TEntity>(string id) where TEntity : class, IDynamicObject
        {
            IRecordReference r;
            var result = FileAccess.Storage.RemoveObject<TEntity>(id);
            if (result)
            {
                r = new JsonRecord
                {
                    DocumentId = id,
                    Revision = "-1"
                };
            }
            else
            {
                r = new JsonRecord
                {
                    DocumentId = id,
                    Revision = "0"
                };
            }
            return r;
        }

        public Task<IRecordReference> Delete<TEntity>(string id) where TEntity : class, IDynamicObject
        {
            return DeleteObject<TEntity>(id).ToTask();
        }

        public Task<IRecordReference> Update<TEntity>(TEntity entityToUpdate) where TEntity : class, IDynamicObject
        {
            var o = FileAccess.Storage.ReplaceObject(entityToUpdate);
            IRecordReference r = new JsonRecord
            {
                DocumentId = o.Id,
                Revision = o.Rev ?? "0",
                Raw = FileAccess.GetJson(o)
            };
            return r.ToTask();
        }
    }
}