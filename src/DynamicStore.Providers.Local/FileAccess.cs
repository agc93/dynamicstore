﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Formatters;
using System.Text;
using DynamicStore.Infrastructure;
using Newtonsoft.Json;

namespace DynamicStore.Providers.Local
{
    internal static class FileAccess
    {
        private static JsonSerializerSettings _settings;
        internal static JsonSerializerSettings Settings => _settings ?? (_settings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.All,
            TypeNameAssemblyFormat = FormatterAssemblyStyle.Full
        });

        internal static class Storage
        {
            

            internal static T StoreObject<T>(T obj) where T : IDynamicObject
            {
                var path = obj.Id.ToStoragePath();
                Access.WriteFile(path, obj);
                return obj;
                //TODO: this should increment the Rev or at least do something!
            }

            internal static T GetObject<T>(T obj) where T : IDynamicObject
            {
                var path = obj.Id.ToStoragePath();
                return Access.ReadFile<T>(path);
            }
            internal static T GetObject<T>(string id) where T : IDynamicObject {
                var path = id.ToStoragePath();
                return Access.ReadFile<T>(path);
            }

            internal static IEnumerable<T> GetObjects<T>() where T : IDynamicObject
            {
                var path = $"{FileConstants.StorageFolder}/";
                var list = Directory.GetFiles(path, "*.json").Select(Access.ReadFile<T>).ToList();
                return list;
            }

            internal static bool RemoveObject<T>(string id)
            {
                var path = id.ToStoragePath();
                try {
                    File.Delete(path);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }

            internal static T ReplaceObject<T>(T obj) where T : IDynamicObject
            {
                var path = obj.Id.ToStoragePath();
                var readFile = Access.ReadFile<T>(path);
                Access.WriteFile<string>(path, null);
                try {
                    Access.WriteFile(path, obj);
                    return obj;
                }
                catch (Exception)
                {
                    Access.WriteFile(path, readFile);
                    return readFile;
                }
                
            }
        }
        internal static class Configuration
        {
            internal static Dictionary<string, KeyValuePair<Type, object>> ReadConfigurationFromFile(string prefix, string folderPrefix = null)
            {
                var path = prefix.ToConfigPath(folderPrefix);
                var dict = Access.ReadFile<Dictionary<string, KeyValuePair<Type, object>>>(path.ToString());
                return dict;
            }

            internal static Dictionary<string, Type> WriteNewConfiguration(Dictionary<string, KeyValuePair<Type, object>> collection, string prefix, string folderPrefix = null)
            {
                var path = prefix.ToConfigPath(folderPrefix);
                Access.WriteFile(path, collection);
                return collection.ToDictionary(k => k.Key, k => k.Value.Key);
            }

            internal static IEnumerable<Dictionary<string, KeyValuePair<Type, object>>> ReadAllConfigurations(string folderPrefix = null) {
                var collection = Directory.GetFiles(folderPrefix ?? FileConstants.StorageFolder, "*.json");
                var dict = collection.Select(prefix => String.Format("{0}/{1}{2}.json", folderPrefix ?? FileConstants.ConfigFolder, prefix, FileConstants.ConfigFileSuffix)).Select(Access.ReadFile<Dictionary<string, KeyValuePair<Type, object>>>).ToList();
                return dict;
            }
        }

        private static class Access
        {
            internal static T ReadFile<T>(string fileName){
                if (!EnsureFileReady(fileName)) throw new FileNotFoundException();
                var json = File.ReadAllText(fileName);
                var deser = JsonConvert.DeserializeObject<T>(json);
                return deser;
            }

            internal static void WriteFile<T>(string fileName, T dictionary) {
                var json = JsonConvert.SerializeObject(dictionary, Formatting.Indented, Settings);
                if (EnsureFileReady(fileName)) {
                    File.WriteAllText(fileName, json);
                } else {
                    throw new FileNotFoundException();
                }
            }

            private static bool EnsureFileReady(string fileName) {
                bool b;
                if (NewFileBehaviour == FileMode.Open) {
                    b = ((File.Exists(fileName)) &&
                         (File.GetAttributes(fileName) != FileAttributes.ReadOnly) &&
                         (File.GetAttributes(fileName) != FileAttributes.Directory));
                } else
                {
                    var fileStream = File.Open(fileName, FileMode.OpenOrCreate);
                    b = fileStream.CanWrite;
                    fileStream.Close();
                }
                if (File.Exists(fileName)) return b;
                try
                {
                    var di = new FileInfo(fileName).Directory;
                    if (!Directory.Exists(di.FullName))
                    {
                        Directory.CreateDirectory(di.FullName);
                    }
                }
                catch
                {
                    // ignored
                    // we tried, and that's what counts
                }
                return b;
            }
        }

        internal static string GetJson<T>(T obj)
        {
            var json = JsonConvert.SerializeObject(obj, Formatting.Indented, Settings);
            return json;
        }

        public static FileMode NewFileBehaviour { private get; set; }
    }

    public static class DictionaryExtensions
    {
        public static DynamicConfiguration ToConfiguration(
            this Dictionary<string, KeyValuePair<Type, object>> collection) {
            var conf = new DynamicConfiguration();
            foreach (var item in collection) {
                conf.Add(item.Key, item.Value);
            }
            return conf;
        }

        internal static string ToConfigPath(this string prefix, string folderPrefix = null)
        {
            var path = String.Format("{0}/{1}{2}.json", folderPrefix ?? FileConstants.ConfigFolder, prefix, FileConstants.ConfigFileSuffix);
            return path;
        }

        internal static string ToStoragePath(this string prefix, string folderPrefix = null)
        {
            var path = String.Format("{0}/{1}{2}.json", folderPrefix ?? FileConstants.StorageFolder, FileConstants.StorageFilePrefix, prefix);
            return path;
        }
    }

    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    internal static class FileConstants
    {
        private static string _configFolder;
        internal static string ConfigFolder
        {
            get { return _configFolder ?? "Configuration"; }
            set { _configFolder = value; }
        }

        private static string _storageFolder;
        internal static string StorageFolder
        {
            get { return _storageFolder ?? "Objects"; }
            set { _storageFolder = value; }
        }

        private static string _storageFile;
        internal static string StorageFilePrefix
        {
            get { return _storageFile ?? "data-"; }
            set { _storageFile = value; }
        }

        private static string _configFile ;
        internal static string ConfigFileSuffix
        {
            get { return _configFile ?? "-config"; }
            set { _configFile = value; }
        }
    }
}