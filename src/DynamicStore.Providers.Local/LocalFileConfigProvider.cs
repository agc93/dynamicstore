﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DynamicStore.Infrastructure;

namespace DynamicStore.Providers.Local
{
    public class LocalFileConfigProvider : IConfigProvider
    {
        public DynamicConfiguration GetConfig(string name)
        {
            FileAccess.NewFileBehaviour = FileMode.OpenOrCreate;
            var configuration = FileAccess.Configuration.ReadConfigurationFromFile(name).ToConfiguration();
            return configuration;
        }

        public IList<DynamicConfiguration> GetAllConfigurations()
        {
            return FileAccess.Configuration.ReadAllConfigurations().Select(c => c.ToConfiguration()).ToList();
        }

        public Dictionary<string, Type> GetConfigSpecification(string name)
        {
            FileAccess.NewFileBehaviour = FileMode.Open;
            var config = FileAccess.Configuration.ReadConfigurationFromFile(name).ToDictionary(k => k.Key, v => v.Value.Key);
            return config;
        }

        public Dictionary<string, Type> AddConfig(string name, DynamicConfiguration configuration)
        {
            var types = FileAccess.Configuration.WriteNewConfiguration(configuration, name);
            return types;
        }
    }
}