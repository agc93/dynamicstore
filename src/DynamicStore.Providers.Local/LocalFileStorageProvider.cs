﻿using System;
using System.Collections.Generic;
using DynamicStore.Infrastructure;
using DynamicStore.Services;
using Newtonsoft.Json;

namespace DynamicStore.Providers.Local
{
    public class LocalFileStorageProvider : IDynamicProvider
    {
        private readonly string _id;

        public LocalFileStorageProvider()
        {
            
        }
        public LocalFileStorageProvider(string id)
        {
            _id = id;
        }

        public DynamicConfiguration DefaultConfiguration => new DynamicConfiguration
        {
            {
                "file:FilePrefix", new KeyValuePair<Type, object>(
                    typeof (string),
                    null)
            },
            {
                "file:FolderPrefix", new KeyValuePair<Type, object>(
                    typeof (string),
                    null)
            }
        };

        public ILogger Log { get; set; }

        public IComplexRepository DynamicRepository => new LocalFileStorageRepository(FilePrefix, FolderPrefix);

        public string Id => _id ?? "localfiles";

        public ISimpleRepository<IDynamicObject> SimpleRepository => new LocalFileStorageStore<IDynamicObject>(FilePrefix, FolderPrefix);

        private string FilePrefix { get; set; }

        private string FolderPrefix { get; set; }

        private List<JsonConverter> Converters { get; set; } = new List<JsonConverter>();

        public void Configure(DynamicConfiguration configuration) {
            try {
                FilePrefix = configuration["file:FilePrefix"].To<string>();
            }
            catch (KeyNotFoundException) {
                FilePrefix = null;
            }
            try
            {
                FolderPrefix = configuration["file:FolderPrefix"].To<string>();
            }
            catch
            {
                FolderPrefix = null;
            }
            try {
                Converters.AddRange(configuration["json:converters"].To<IEnumerable<JsonConverter>>());
            }
            catch {
                // ignored
            }
            try {
                Converters.Add(configuration["json:converter"].To<JsonConverter>());
            }
            catch {
                // ignored
            }
        }
    }
}