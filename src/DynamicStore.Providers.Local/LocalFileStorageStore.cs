﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DynamicStore.Infrastructure;
using DynamicStore.Services;

namespace DynamicStore.Providers.Local
{
    public class LocalFileStorageStore<TEntity> : LocalFileBaseRepository, ISimpleRepository<TEntity> where TEntity : class, IDynamicObject
    {

        public LocalFileStorageStore(string filePrefix, string folderPrefix)
        {
            if (filePrefix.IsNotNull()) {
                FilePrefix = filePrefix;
            }
            if (folderPrefix.IsNotNull()) {
                FolderPrefix = folderPrefix;
            }
        }


        public Task<IEnumerable<TEntity>> Get()
        {
            IEnumerable<TEntity> entities = base.Get<TEntity>();
            return entities.ToTask();
        }

        public Task<TEntity> GetById(string id)
        {
            return GetById<TEntity>(id).ToTask();
        }

        public Task<IRecordReference> Create(TEntity entity)
        {
            return base.Create(entity).ToTask();
        }

        public Task<IRecordReference> Delete(TEntity entityToDelete)
        {
            return base.Delete(entityToDelete).ToTask();
        }

        public Task<IRecordReference> Delete(string id)
        {
            return base.Delete<TEntity>(id).ToTask();
        }

        public Task<IRecordReference> Update(TEntity entityToUpdate)
        {
            return base.Update(entityToUpdate).ToTask();
        }
    }
}