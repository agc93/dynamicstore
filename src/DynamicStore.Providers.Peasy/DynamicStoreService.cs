using Peasy.Core;

namespace DynamicStore.Providers.Peasy
{
    public class DynamicStoreService<T> : ServiceBase<T, string> where T : DynamicDomainObject, new()
    {
        public DynamicStoreService(IDataProxy<T, string> dataProxy) : base(dataProxy)
        {

        }
    }
}