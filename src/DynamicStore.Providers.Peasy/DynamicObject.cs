﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DynamicStore.Infrastructure;
using Peasy.Core;

namespace DynamicStore.Providers.Peasy
{
    public abstract class DynamicDomainObject : IDynamicObject, IDomainObject<string>
    {
        public string Id => ID;
        public abstract string Rev { get; set; }
        public abstract string ID { get; set; }
    }
}
