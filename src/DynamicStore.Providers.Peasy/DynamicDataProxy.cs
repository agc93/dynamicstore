﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DynamicStore.Infrastructure;
using Peasy.Core;

namespace DynamicStore.Providers.Peasy
{
    /// <summary>
    /// An implementation of the Peasy DataProxy pattern for users who want to use DynamicStore as a backing store for Peasy.NET
    /// </summary>
    /// <typeparam name="T"></typeparam>
    class DynamicDataProxy<T> : IDataProxy<T, string> where T : class, IDynamicObject, IDomainObject<string>
    {
        public DynamicDataProxy(Stores.DynamicStore store)
        {
            Store = store;
        }

        private Stores.DynamicStore Store { get; set; }

        public IEnumerable<T> GetAll()
        {
            return Store.Objects.Get<T>().Result;
        }

        public T GetByID(string id)
        {
            return Store.Objects.GetById<T>(id).Result;
        }

        public T Insert(T entity)
        {
            var record = Store.Objects.Create(entity).Result;
            return GetByID(record.Id);
        }

        public T Update(T entity)
        {
            var record = Store.Objects.Update(entity).Result;
            return GetByID(record.Id);
        }

        public void Delete(string id)
        {
            var recordReference = Store.Objects.Delete<T>(id).Result;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var list = await Store.Objects.Get<T>();
            return list.AsEnumerable();
        }

        public Task<T> GetByIDAsync(string id)
        {
            return Store.Objects.GetById<T>(id);
        }

        public async Task<T> InsertAsync(T entity)
        {
            var record = await Store.Objects.Create(entity);
            return GetByID(record.Id);
        }

        public async Task<T> UpdateAsync(T entity)
        {
            var record = await Store.Objects.Update(entity);
            return GetByID(record.Id);
        }

        public Task DeleteAsync(string id)
        {
            return Store.Objects.Delete<T>(id);
        }
    }
}
