﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DynamicStore.Infrastructure;

namespace DynamicStore.Providers.Peasy
{
    public class PeasyStorageProvider : IDynamicProvider
    {
        public PeasyStorageProvider()
        {
            
        }

        public PeasyStorageProvider(DynamicConfiguration configuration) : this()
        {
            
        }
        public string Id => "Peasy.NET";

        public IComplexRepository DynamicRepository
        {
            get { throw new NotImplementedException(); }
        }

        public ISimpleRepository<IDynamicObject> SimpleRepository
        {
            get { throw new NotImplementedException(); }
        }

        public void Configure(DynamicConfiguration configuration)
        {
            throw new NotImplementedException();
        }

        public DynamicConfiguration DefaultConfiguration
        {
            get { throw new NotImplementedException(); }
        }

        public ILogger Log
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }
    }
}
