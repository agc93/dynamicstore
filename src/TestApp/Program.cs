﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using DynamicStore;
using DynamicStore.Infrastructure;
using DynamicStore.Providers;
using DynamicStore.Providers.AppConfig;
using DynamicStore.Providers.Local;
using DynamicStore.Services;

namespace TestApp
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal class Program
    {
        private static void Main(string[] args) {
            //var conf = new DynamicConfiguration();
            //var conf = new DynamicConfiguration
            //{
            //    {
            //        "key1",
            //        new KeyValuePair<Type, object>(typeof (SampleObject),
            //            new SampleObject {Id = "testid", Rev = "testrev"})
            //    },
            //    {
            //        "key2",
            //        new KeyValuePair<Type, object>(typeof (ForeignObject),
            //            new ForeignObject {Id = "foreignid", Rev = "foreignrev", IsBroken = true, Number = 23.599})
            //    }
            //};
            var conf = new DynamicConfiguration()
            {
                {
                    "file:FilePrefix", new KeyValuePair<Type, object>(typeof (string), "store-")
                },
                {
                    "file:FolderPrefix", new KeyValuePair<Type, object>(typeof (string), "Objects")
                }
            };
            var store = new DynamicStoreEngine()
                .EnableConfiguration(new AppConfigProvider())
                .EnableStorageProvider(new LocalFileStorageProvider())
                .HideExceptionDetails(true)
                .UseComplexStore();
            store.Config.Source.AddConfig("local-new", conf);
            var obj = store.Objects.Create(new SampleObject { Id = "complexTest", Rev = "4" });
            var simple = new DynamicStoreEngine()
                .EnableStorageProvider(new LocalFileStorageProvider())
                .EnableConfiguration(new EnvironmentVarConfigProvider())
                .UseSimpleStore<SampleObject>();
            Console.ReadLine();
            var objs = store.Objects.Get<SampleObject>();
            var newConf = new DynamicConfiguration()
            {
                {
                    "db:UserName", new KeyValuePair<Type, object>(typeof (string), "testuser")
                },
                {
                    "db:Password", new KeyValuePair<Type, object>(typeof (string), "testpassword")
                },
                {
                    "db:Uri", new KeyValuePair<Type, object>(typeof (Uri), new Uri("cloud.cloudant.com"))
                },
                {
                    "db:DbName", new KeyValuePair<Type, object>(typeof (string), "myAwesomeDB")
                }
            };
        }

        private static async void TestMethod(DynamicStore.Stores.DynamicStore dynamicStore) {
            var myObjects = await dynamicStore.Objects.GetById<SampleObject>("");
            var record = await dynamicStore.Objects.Create(myObjects);
        }

        private class SampleObject : IDynamicObject
        {
            public string Id { get; set; }

            public string Rev { get; set; }
        }

        private class ForeignObject : IDynamicObject
        {
            public string Id { get; set; }

            public string Rev { get; set; }

            public bool IsBroken { get; set; }

            public double Number { get; set; }
        }
    }
}