﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("DynamicStore.Net45")]
[assembly: AssemblyDescription("The .NET 4.5 version of the core DynamicStore library, including everything you need to get up and running with DynamicStore storage in your application")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Alistair Chapman")]
[assembly: AssemblyProduct("DynamicStore.Net45")]
[assembly: AssemblyCopyright("Copyright ©  2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("141a13b7-c606-43b3-b37b-39038de8ab4b")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("0.9.1.1")]
[assembly: AssemblyFileVersion("0.9.1.1")]
