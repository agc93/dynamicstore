﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DynamicStore.Infrastructure;

namespace DynamicStore.Services
{
    internal partial class DefaultLogger
    {
        private static readonly string DefaultPath = Environment.GetFolderPath(
            Environment.SpecialFolder.ApplicationData, Environment.SpecialFolderOption.Create);
        public void Write(string message)
        {
            File.AppendAllText(DefaultPath, String.Format("{0}| Error encountered! {1} EE: {2}", Prefix, Environment.NewLine, message));
        }
    }
}
