﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DynamicStore.Infrastructure;

namespace DynamicStore.Services
{
    public class EnvironmentVarConfigProvider : IConfigProvider
    {
        public DynamicConfiguration GetConfig(string name) {
            return GetVariables();
        }

        private static DynamicConfiguration GetVariables() {
            var variables = Environment.GetEnvironmentVariables();
            var conf = new DynamicConfiguration();
            foreach (DictionaryEntry item in variables) {
                conf.Add(item.Key.ToString(), new KeyValuePair<Type, object>(typeof(string), item.Value));
            }
            return conf;
        }

        public IList<DynamicConfiguration> GetAllConfigurations() {
            return new List<DynamicConfiguration>
            {
                GetVariables()
            };
        }

        public Dictionary<string, Type> GetConfigSpecification(string name) {
            var vars = GetVariables();
            return vars.ToDictionary(item => item.Key.ToString(), item => typeof(string));
        }

        public Dictionary<string, Type> AddConfig(string name, DynamicConfiguration configuration) {
            foreach (var item in configuration) {
                Environment.SetEnvironmentVariable(item.Key, item.Value.Value.ToString());
            }
            return GetConfigSpecification(String.Empty);
        }
    }
}