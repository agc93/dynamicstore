﻿using System.Diagnostics;

namespace DynamicStore.Services
{
    partial class DefaultLogger
    {
        public void Write(string message) {
            if (message != null) Debug.WriteLine(message);
        }
    }
}