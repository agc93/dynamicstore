﻿using System;
using System.Configuration;
// ReSharper disable ClassNeverInstantiated.Global

namespace DynamicStore.Providers.AppConfig
{
    internal class DynamicStoreConfigSection : ConfigurationSection
    {
        /// <summary>
        /// The name of this section in the app.config.
        /// </summary>
        public const string SectionName = "DynamicStoreConfigSection";

        private const string ConfigCollectionName = "DynamicStoreConfigs";

        [ConfigurationProperty("", IsDefaultCollection = true, IsKey = false, IsRequired = true)]
        public DynamicStoreConfigCollection DynamicStoreConfigs { get { return (DynamicStoreConfigCollection)base[""]; } }
        
        [ConfigurationProperty("name", IsRequired = false)]
        public string Name {
            get { return (string)base["name"]; }
            set { base["name"] = value; }
        }
    }

    internal class DynamicStoreConfigCollection : ConfigurationElementCollection
    {
        //so this should be a collection of Config (inside the DynamicStoreConfigSection)
        // each element should be a new config (i.e. gettable from a ConfigProvider)
        protected override ConfigurationElement CreateNewElement() {
            return new DynamicStoreConfig();
        }

        protected override string ElementName {
            get { return "provider"; }
        }

        protected override bool IsElementName(string elementName) {
            return !String.IsNullOrEmpty(elementName) && elementName == ElementName;
        }

        public override ConfigurationElementCollectionType CollectionType {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        protected override object GetElementKey(ConfigurationElement element) {
            return ((DynamicStoreConfig)element).Name;
        }

        public DynamicStoreConfig this[int index] {
            get { return BaseGet(index) as DynamicStoreConfig; }
        }

        public new DynamicStoreConfig this[string key] {
            get { return BaseGet(key) as DynamicStoreConfig; }
        }

        [ConfigurationProperty("name", IsRequired = true)]
        public string Name {
            get { return (string)base["name"]; }
            set { base["name"] = value; }
        }

        #region Methods

        public void Add(DynamicStoreConfig config) {
            BaseAdd(config);
        }

        public void Remove(string name) {
            BaseRemove(name);
        }

        public void Remove(DynamicStoreConfig config) {
            BaseRemove(GetElementKey(config));
        }

        public void Clear() {
            BaseClear();
        }

        public void RemoveAt(int index) {
            BaseRemoveAt(index);
        }

        public string GetKey(int index) {
            return (string)BaseGetKey(index);
        }

        #endregion Methods
    }

    [ConfigurationCollection(typeof(ConfigurationElement), AddItemName = "item", CollectionType = ConfigurationElementCollectionType.BasicMap)]
    internal class DynamicStoreConfig : ConfigurationElementCollection
    {
        //here is the actual config
        //it is just a collection of elements as well
        //trust me, I know

        //this should have an ATTRIBUTE for name
        //and MULTIPLE PROPERTIES for items (DynamicStoreConfigElements?)
        // ^^ which may require this being a ConfigurationElementCollection...

        //Basically, this should be convertible to a DynamicConfiguration object
        // which will be hard enough without all this XML bullshit
        protected override ConfigurationElement CreateNewElement() {
            return new DynamicStoreConfigElement();
        }

        protected override string ElementName {
            get { return "item"; }
        }

        protected override bool IsElementName(string elementName) {
            return !String.IsNullOrEmpty(elementName) && elementName == ElementName;
        }

        public override ConfigurationElementCollectionType CollectionType {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        protected override object GetElementKey(ConfigurationElement element) {
            return ((DynamicStoreConfigElement)element).Key;
        }

        public DynamicStoreConfigElement this[int index] {
            get { return BaseGet(index) as DynamicStoreConfigElement; }
        }

/*
        public DynamicStoreConfigElement this[string key] {
            get { return BaseGet(key) as DynamicStoreConfigElement; }
        }
*/

        [ConfigurationProperty("name", IsRequired = true)]
        public string Name {
            get { return (string)base["name"]; }
            set { base["name"] = value; }
        }

        #region Methods

        public void Add(DynamicStoreConfigElement item) {
            BaseAdd(item);
        }

        public void Remove(string name) {
            BaseRemove(name);
        }

        public void Remove(DynamicStoreConfigElement item) {
            BaseRemove(GetElementKey(item));
        }

        public void Clear() {
            BaseClear();
        }

        public void RemoveAt(int index) {
            BaseRemoveAt(index);
        }

        public string GetKey(int index) {
            return (string)BaseGetKey(index);
        }

        #endregion Methods
    }

    internal class DynamicStoreConfigElement : ConfigurationElement
    {
        //this is an actual item. The sort of shit you get all index-y with.
        //that means this would be an ENTRY in a DynamicConfiguration
        [ConfigurationProperty("key", IsRequired = true)]
        public string Key {
            get { return (string)this["key"]; }
            set { this["key"] = value; }
        }

        [ConfigurationProperty("value", IsRequired = true)]
        public string Value {
            get { return this["value"].ToString(); }
            set { this["value"] = value; }
        }
    }
}