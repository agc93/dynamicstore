﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using DynamicStore.Infrastructure;
using DynamicStore.Services;

namespace DynamicStore.Providers.AppConfig
{
    public class AppConfigProvider : IConfigProvider
    {
        public AppConfigProvider()
        {

        }
        public AppConfigProvider(string exePath)
        {
            ConfigurationFileMap map = new ConfigurationFileMap(exePath);
            _config = ConfigurationManager.OpenMappedMachineConfiguration(map);
        }

        private static DynamicStoreConfigSection Section
            => InstanceConfiguration.GetSection("DynamicStoreConfigSection") as DynamicStoreConfigSection;

        private static Configuration _config;
        private static Configuration InstanceConfiguration => _config ?? (_config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None));

        public DynamicConfiguration GetConfig(string name)
        {
            var config = Section.DynamicStoreConfigs[name];
            return CreateConfiguration(config);
        }

        public IList<DynamicConfiguration> GetAllConfigurations() {
            IList<DynamicConfiguration> list = new List<DynamicConfiguration>();
            var confs = Section.DynamicStoreConfigs;
            for (var i = 0; i < confs.Count; i++) {
                list.Add(CreateConfiguration(confs[i]));
            }
            return list;
        }

        public Dictionary<string, Type> GetConfigSpecification(string name) {
            var dict = new Dictionary<string, Type>();
            var config = Section.DynamicStoreConfigs[name];
            for (var i = 0; i < config.Count; i++) {
                dict.Add(config[i].Key, config[i].Value.GetType());
            }
            return dict;
        }

        public Dictionary<string, Type> AddConfig(string name, DynamicConfiguration configuration)
        {
            Section.DynamicStoreConfigs.Add(CreateConfiguration(configuration, name));
            InstanceConfiguration.Save();
            return GetConfigSpecification(name);
        }

        private static DynamicConfiguration CreateConfiguration(DynamicStoreConfig config) {
            var conf = new DynamicConfiguration();
            for (var i = 0; i < config.Count; i++) {
                var val = config[i].Value;
                conf.Add(config[i].Key, new KeyValuePair<Type, object>(val.GetType(), config[i].Value));
            }
            return conf;
        }

        private static DynamicStoreConfig CreateConfiguration(DynamicConfiguration config, string name = null) {
            var conf = new DynamicStoreConfig()
            {
                Name = name ?? ""
            };
            foreach (var item in config) {
                conf.Add(new DynamicStoreConfigElement { Key = item.Key, Value = item.Value.Value.ToString() });
            }
            return conf;
        }
    }
}