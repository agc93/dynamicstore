﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("DynamicStore.Providers.AppConfig")]
[assembly: AssemblyDescription("A DynamicStore configuration provider using custom sections in app.config to control any storage backend")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Alistair Chapman")]
[assembly: AssemblyProduct("DynamicStore.Providers.AppConfig")]
[assembly: AssemblyCopyright("Copyright ©  2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("892d649b-e800-4ba2-a5fe-0b9817a7b2c1")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("0.9.1.4")]
[assembly: AssemblyFileVersion("0.9.1.4")]
